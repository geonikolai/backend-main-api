﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Facebook;

namespace BubbllzWeb.Api.Controllers
{
    public static class Helpers
    {
        public static string getFbUserId(string accessToken)
        {

            try
            {
                var fb = new FacebookClient(accessToken);
                var result = (IDictionary<string, object>)fb.Get("/me?fields=id");
                return (string)result["id"];
            }
            catch (FacebookOAuthException)
            {
                return null;
            }
        }

    }
}