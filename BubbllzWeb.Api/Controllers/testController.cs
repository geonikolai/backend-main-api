﻿using BubbllzWeb.Api.Models;
using BubbllzWeb.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BubbllzWeb.Services.Services.FaceBookServices;
using BubbllzWeb.Domain.AppContext;
using Newtonsoft.Json;
using BubbllzWeb.Services.ApiModels;

namespace BubbllzWeb.Api.Controllers
{
    [RoutePrefix("testapi")]
    public class testapiController : ApiController
    {
             



        [HttpGet]
        [Route("test2")]
        public async Task<IHttpActionResult> fbget()
        {
            dynamic result;
            BoomUserServices service = new BoomUserServices();

            using (MysqlContext context = new MysqlContext())
            {
                var users = (from u in context.boom_user.Where(u => u.id > 500)
                             select u.id);
                result = JsonConvert.SerializeObject(users);
            }
            return Ok(result);

        }



        [HttpGet]
        [Route("test1/{userId}")]
        public async Task<IHttpActionResult> GetPendingBubbllz(int userId)
        {
            using (var service = new ApiService())
            {
                return Ok(await service.GetPendingBubbllz(userId));
            }
        }

        [HttpGet]
        [Route("test3/{storeId}/{userId}")]
        public async Task<IHttpActionResult> HasPendingBubbllz(int storeId, int userId)
        {
            using (var service = new ApiService())
            {
                return Ok(await service.HasPendingBubbllz(storeId, userId));
            }
        }

        [HttpGet]
        [Route("test4/{storeId}/{userId}")]
        public async Task<IHttpActionResult> CreateNewBubbll(int storeId, int userId)
        {
            using (var service = new ApiService())
            {
                return Ok(await service.HasPendingBubbllz(storeId, userId));
            }
        }


        

    }
}
