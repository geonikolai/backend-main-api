﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BubbllzWeb.Domain.AppContext
{
    [XmlRoot("ServerSettings")]
    public class DbSettings
    {
        [XmlElement("AppId")]
        public String AppId { get; set; }

        [XmlElement("ProviderName")]
        public string ProviderName { get; set; }

        [XmlElement("Server")]
        public String Server { get; set; }

        [XmlElement("Database")]
        public String Database { get; set; }

        [XmlElement("User")]
        public String User { get; set; }

        [XmlElement("Password")]
        public String Password { get; set; }

        [XmlElement("Store")]
        public String Store { get; set; }

        [XmlElement("PriceList")]
        public String PriceList { get; set; }




        public EntityConnectionStringBuilder CreateConnectionString()
        {
            SqlConnectionStringBuilder SqlBuilder = new SqlConnectionStringBuilder();
            EntityConnectionStringBuilder EntityBuilder = new EntityConnectionStringBuilder();

            SqlBuilder.DataSource = this.Server;
            SqlBuilder.InitialCatalog = this.Database;
            SqlBuilder.UserID = this.User;
            SqlBuilder.Password = this.Password;

            EntityBuilder.ProviderConnectionString = SqlBuilder.ToString();

            return EntityBuilder;
        }

        public static DbSettings RefreshSettings()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DbSettings));
            DbSettings settings = new DbSettings();
            string a = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName.ToString();
            using (FileStream fileStream = new FileStream(Path.Combine(a, @"ServerSettings.xml").ToString(), FileMode.Open))

            {
                settings = (DbSettings)serializer.Deserialize(fileStream);

            }
            return settings;
        }


    }
}
