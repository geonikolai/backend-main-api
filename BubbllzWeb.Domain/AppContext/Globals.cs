﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Domain.AppContext
{
    public sealed class Globals
    {

        private static volatile Globals instance;
        private static object objectlockCheck = new Object();
        private Globals() { }

        public static Globals GetInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (objectlockCheck)
                    {
                        if (instance == null)
                            instance = new Globals();
                    }
                }

                return instance;
            }
        }
        


    }
}
