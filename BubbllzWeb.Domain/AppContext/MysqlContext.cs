﻿using System.Data.Entity;
using BubbllzWeb.Domain.DbModels;
using Microsoft.AspNet.Identity.EntityFramework;
using BubbllzWeb.Domain.AppContext;

namespace BubbllzWeb.Domain.AppContext
{

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class MysqlContext : IdentityDbContext
    {

        public MysqlContext() : base(Registry.EntityBuilder.ProviderConnectionString.ToString())
        {
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<MysqlContext>(null);
            Database.CommandTimeout = 150000;
        }
        
        public DbSet<Boom> boom { get; set; }
        public DbSet<BoomUser> boom_user { get; set; }

        
    }

}
