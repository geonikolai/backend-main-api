﻿
using BubbllzWeb.Domain.AppContext;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Web;

namespace BubbllzWeb.Domain.AppContext
{
    public sealed class Registry
    {
        private static DbSettings settings;
        public static  DbSettings Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = DbSettings.RefreshSettings();
                }

                return settings;
            }
        }

        private static EntityConnectionStringBuilder entitybuilder;
        public static EntityConnectionStringBuilder EntityBuilder
        {
            get
            {
                entitybuilder = Settings.CreateConnectionString();

                return entitybuilder;
            }
        }

        //private static FacebookAppSettings facebokAppSettings;
        //public static FacebookAppSettings FacebookAppSettings
        //{
        //    get
        //    {
        //        if (facebokAppSettings == null)
        //        {
        //            facebokAppSettings = FacebookAppSettings;
        //        }

        //        return facebokAppSettings;
        //    }
        //}






    }
}
