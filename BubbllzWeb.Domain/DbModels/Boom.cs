namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;


    [DataContract]
    [Table("boom")]
    public class Boom
    {

        [Key, Required]
        public int id { get; set; }
        [Required]
        public int boom_user_id { get; set; }

        public int status { get; set; }

        [Required]
        public int store_id { get; set; }

        public int checkin_id { get; set; }

        public int boom_category_id { get; set; }

        public int brand_id { get; set; }

        public int coupon_id { get; set; }

        public string image_name { get; set; }

        public string boom_user_message { get; set; }

        public DateTime c_date { get; set; }

        public DateTime p_date { get; set; }
    }
}
