namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    [DataContract]
    [Table("boom_user")]
    public class BoomUser
    {

        public BoomUser()
        {


        }

        public int id { get; set; }

        [Required]
        [StringLength(45)]
        public string username { get; set; }

        [StringLength(100)]
        public string firstname { get; set; }

        [StringLength(100)]
        public string lastname { get; set; }

        [Required]
        [StringLength(100)]
        public string email { get; set; }

        [StringLength(100)]
        public string email2 { get; set; }

        [Required]
        [StringLength(64)]
        public string password { get; set; }

        [StringLength(100)]
        public string superboomuser_name { get; set; }

        [StringLength(32)]
        public string gender { get; set; }

        public DateTime? birth { get; set; }

        public int? min_age { get; set; }

        public int? max_age { get; set; }

        [StringLength(15)]
        public string postcode { get; set; }

        [StringLength(20)]
        public string telephone { get; set; }

        [StringLength(20)]
        public string mobile_phone { get; set; }

        public int? state_id { get; set; }

        public int? country_id { get; set; }

        public int? city_id { get; set; }

        [StringLength(60)]
        public string region { get; set; }

        [StringLength(45)]
        public string address { get; set; }

        [StringLength(45)]
        public string address2 { get; set; }

        [StringLength(255)]
        public string profile_image { get; set; }

        [StringLength(255)]
        public string profile_image_file { get; set; }

        public bool? is_superboomuser { get; set; }

        public bool? status { get; set; }

        [StringLength(64)]
        public string status_data { get; set; }

        public DateTime? registration_date { get; set; }

        public DateTime? last_login_date { get; set; }

        public DateTime? last_modified_date { get; set; }

        public int? count_booms { get; set; }

        public int? points { get; set; }

        public int? fb_likes { get; set; }

        public int? tw_likes { get; set; }

        public int? inst_likes { get; set; }

        public int? pinter_likes { get; set; }

        public int? boom_likes { get; set; }

        public int? coupon_counts { get; set; }

        public int? fb_booms { get; set; }

        public int? tw_booms { get; set; }

        public int? inst_booms { get; set; }

        public int? pinter_booms { get; set; }

        public int? fb_views { get; set; }

        public int? tw_views { get; set; }

        public int? inst_views { get; set; }

        public int? pinter_views { get; set; }

        public int? fb_shares { get; set; }

        public int? tw_shares { get; set; }

        public int? inst_shares { get; set; }

        public int? pinter_shares { get; set; }

        public float? total_causes { get; set; }

        public int? count_causes { get; set; }

        public int? loyalty_points { get; set; }

        public float? total_boom_discount { get; set; }

        public int? count_boom_discount { get; set; }

        public float? total_coupon_discount { get; set; }

        public int? count_coupon_discount { get; set; }

        [StringLength(100)]
        public string fb_id { get; set; }

        [StringLength(64)]
        public string os_version { get; set; }

        [StringLength(64)]
        public string device_model { get; set; }

        [StringLength(64)]
        public string manufacturer { get; set; }

        [Required]
        [StringLength(255)]
        public string username_canonical { get; set; }

        [Required]
        [StringLength(255)]
        public string email_canonical { get; set; }

        public bool enabled { get; set; }

        [Required]
        [StringLength(255)]
        public string salt { get; set; }

        public DateTime? last_login { get; set; }

        public bool locked { get; set; }

        public bool expired { get; set; }

        public DateTime? expires_at { get; set; }

        [StringLength(255)]
        public string confirmation_token { get; set; }

        public DateTime? password_requested_at { get; set; }

        [StringLength(1073741823)]
        public string roles { get; set; }

        public bool credentials_expired { get; set; }

        public DateTime? credentials_expire_at { get; set; }

        [StringLength(255)]
        public string facebook_access_token { get; set; }

        public bool itbooms { get; set; }

    }
}
