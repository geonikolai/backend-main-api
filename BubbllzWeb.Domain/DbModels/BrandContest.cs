namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

   
    public partial class BrandContest
    {
        
        public BrandContest()
        {
            boom = new HashSet<Boom>();
            brandContestExclusions = new HashSet<BrandContestExclusions>();
            brandContestWinners = new HashSet<BrandContestWinners>();
            contest = new HashSet<contest>();
        }

        public int id { get; set; }

        public int brand_id { get; set; }

        public int lists_id { get; set; }

        [Required]
        [StringLength(64)]
        public string title { get; set; }

        [Required]
        [StringLength(255)]
        public string notification { get; set; }

        [Required]
        [StringLength(255)]
        public string description { get; set; }

        [StringLength(100)]
        public string before_checkin_message { get; set; }

        [StringLength(100)]
        public string after_checkin_message { get; set; }

        public DateTime start_date { get; set; }

        public DateTime end_date { get; set; }

        public DateTime c_date { get; set; }

        public bool unattended { get; set; }

        public bool remote { get; set; }

        public int winners { get; set; }

        public int status { get; set; }

        public int entries { get; set; }

        public DateTime? last_update { get; set; }

       
        public virtual ICollection<Boom> boom { get; set; }

       
        public virtual ICollection<BrandContestExclusions> brandContestExclusions { get; set; }

      

   
        public virtual ICollection<BrandContestWinners> brandContestWinners { get; set; }
        
        public virtual ICollection<contest> contest { get; set; }
    }
}
