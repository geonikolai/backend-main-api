namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    
    public partial class BrandContestExclusions
    {
        public int id { get; set; }

        public int boomUserId { get; set; }

        public int brandContestId { get; set; }

        public DateTime cdate { get; set; }

        public virtual BoomUser boom_user { get; set; }

        public virtual BrandContest brand_contest { get; set; }
    }
}
