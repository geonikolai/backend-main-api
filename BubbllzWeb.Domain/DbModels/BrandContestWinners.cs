namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class BrandContestWinners
    {
        public int id { get; set; }

        public int boom_user_id { get; set; }

        public int boom_id { get; set; }

        public int store_id { get; set; }

        public int brand_contest_id { get; set; }

        public DateTime draw_date { get; set; }

        [StringLength(64)]
        public string prize { get; set; }

        public virtual Boom boom { get; set; }

        public virtual BoomUser boom_user { get; set; }

        public virtual BrandContest brand_contest { get; set; }

        public virtual Store store { get; set; }
    }
}
