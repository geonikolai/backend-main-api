namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

  
    public partial class contest
    {
        
        public contest()
        {
            boom = new HashSet<Boom>();
            contestWinners = new HashSet<ContestWinners>();
        }

        public int id { get; set; }

        public int store_id { get; set; }

        [Required]
        [StringLength(64)]
        public string title { get; set; }

        [Required]
        [StringLength(255)]
        public string notification { get; set; }

        [Required]
        [StringLength(255)]
        public string description { get; set; }

        [StringLength(100)]
        public string before_checkin_message { get; set; }

        [StringLength(100)]
        public string after_checkin_message { get; set; }

        public DateTime? start_date { get; set; }

        public DateTime? end_date { get; set; }

        public DateTime? c_date { get; set; }

        public bool unattended { get; set; }

        public bool remote { get; set; }

        public int winners { get; set; }

        public int status { get; set; }

        public int entries { get; set; }

        public int? brand_contest_id { get; set; }

        public DateTime? last_update { get; set; }

        
        public virtual ICollection<Boom> boom { get; set; }

        public virtual BrandContest brandContest { get; set; }
        

        public virtual Store store { get; set; }

       
        public virtual ICollection<ContestWinners> contestWinners { get; set; }
    }
}
