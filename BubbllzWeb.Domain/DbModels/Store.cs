namespace BubbllzWeb.Domain.DbModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    public partial class Store
    {

        public Store()
        {


        }

        public int id { get; set; }

        [Required]
        [StringLength(100)]
        public string storename { get; set; }

        public int? afm { get; set; }

        [StringLength(255)]
        public string real_name { get; set; }

        [StringLength(100)]
        public string doy { get; set; }

        [StringLength(255)]
        public string logo_image { get; set; }

        [StringLength(255)]
        public string main_image { get; set; }

        [StringLength(255)]
        public string keywords { get; set; }

        [StringLength(255)]
        public string hash_tags { get; set; }

        [StringLength(255)]
        public string perma_tag { get; set; }

        [StringLength(100)]
        public string broadcast_message { get; set; }

        [StringLength(100)]
        public string store_general_message { get; set; }

        [StringLength(100)]
        public string slogan { get; set; }

        public DateTime c_date { get; set; }

        public DateTime? confirmation_date { get; set; }

        public DateTime? completion_date { get; set; }

        public int? type_store { get; set; }

        [StringLength(45)]
        public string email { get; set; }

        [StringLength(20)]
        public string telephone { get; set; }

        public int? category_of { get; set; }

        public int? state_id { get; set; }

        [StringLength(45)]
        public string state_title { get; set; }

        public int? country_id { get; set; }

        [StringLength(45)]
        public string country_title { get; set; }

        public int? organization_id { get; set; }

        public int? marketplace_id { get; set; }

        [StringLength(45)]
        public string marketplace_title { get; set; }

        public int? reseller_id { get; set; }

        public int? area_id { get; set; }

        public int? city_id { get; set; }

        [StringLength(45)]
        public string city_title { get; set; }

        [StringLength(45)]
        public string region { get; set; }

        [StringLength(60)]
        public string address { get; set; }

        [StringLength(60)]
        public string address2 { get; set; }

        public bool? is_boom_address { get; set; }

        [StringLength(20)]
        public string vat_number { get; set; }

        [StringLength(13)]
        public string postcode { get; set; }

        public decimal? longitute { get; set; }

        public decimal? latitude { get; set; }

        [StringLength(255)]
        public string bing_map_link { get; set; }

        [StringLength(255)]
        public string google_map_link { get; set; }

        public bool? permit_broadcast_message { get; set; }

        public bool? permit_diff_logo_image { get; set; }

        public int? count_booms { get; set; }

        public int? points { get; set; }

        public int? fb_likes { get; set; }

        public int? tw_likes { get; set; }

        public int? inst_likes { get; set; }

        public int? pinter_likes { get; set; }

        public int? coupon_counts { get; set; }

        public int? fb_booms { get; set; }

        public int? tw_booms { get; set; }

        public int? inst_booms { get; set; }

        public int? pinter_booms { get; set; }

        public int? fb_views { get; set; }

        public int? tw_views { get; set; }

        public int? inst_views { get; set; }

        public int? pinter_views { get; set; }

        public int? fb_shares { get; set; }

        public int? tw_shares { get; set; }

        public int? inst_shares { get; set; }

        public int? pinter_shares { get; set; }

        public float? total_causes { get; set; }

        public int? count_causes { get; set; }

        public int? loyalty_points { get; set; }

        public float? total_boom_discount { get; set; }

        public int? count_boom_discount { get; set; }

        public float? total_coupon_discount { get; set; }

        public int? count_coupon_discount { get; set; }

        public sbyte? status { get; set; }

        [StringLength(255)]
        public string fb_page_id { get; set; }

        public DateTime? last_update { get; set; }


    }
}
