﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Services.ApiModels
{
    public class ApiResponse
    {
        public string data { get; set; }
        public string response { get; set; }
        public int status { get; set; }
    }
}
