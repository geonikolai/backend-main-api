﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Services.DTO
{

    [DataContract]
    public class BoomDTO
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public int boom_user_id { get; set; }

        [DataMember]
        public int status { get; set; }

        [DataMember]
        public int store_id { get; set; }

        [DataMember]
        public int checkin_id { get; set; }

        [DataMember]
        public int boom_category_id { get; set; }

        [DataMember]
        public int brand_id { get; set; }

        [DataMember]
        public int coupon_id { get; set; }

        [DataMember]
        public string image_name { get; set; }

        [DataMember]
        public string boom_user_message { get; set; }

        [DataMember]
        public DateTime c_date { get; set; }

        [DataMember]
        public DateTime p_date { get; set; }



    }
}
