﻿

using BubbllzWeb.Domain.AppContext;
using BubbllzWeb.Domain.DbModels;
using BubbllzWeb.Services.ApiModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Services.Repositories
{
    public class UnitOfWork : IDisposable
    {


        private DbContext _context = null;
        private GenericRepository<Boom> _boomRepository;
        private GenericRepository<Store> _storeRepository;
        private GenericRepository<BoomUser> _boomUserRepository;


        public UnitOfWork()
        {
            _context = new DbContext(Registry.EntityBuilder.ProviderConnectionString.ToString());
        }



        public GenericRepository<Boom> BoomRepository
        {
            get
            {
                if (this._boomRepository == null)
                    this._boomRepository = new GenericRepository<Boom>(_context);
                return _boomRepository;
            }
        }
        
        public GenericRepository<BoomUser> BoomUserRepository
        {
            get
            {
                if (this._boomUserRepository == null)
                    this._boomUserRepository = new GenericRepository<BoomUser>(_context);
                return _boomUserRepository;
            }
        }
        
        public GenericRepository<Store> StoreRepository
        {
            get
            {
                if (this._storeRepository == null)
                    this._storeRepository = new GenericRepository<Store>(_context);
                return _storeRepository;
            }
        }




        
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                //GIA TON LOGGER
                //System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }

        }



        private bool disposed = false;


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

     
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
