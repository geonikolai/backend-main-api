﻿using BubbllzWeb.Domain.DbModels;
using BubbllzWeb.Services.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace BubbllzWeb.Services.Services
{

    // TA BASE ACTIONS GIA TO BOOM
    public class BoomServices : IBoomService
    {
        private readonly UnitOfWork _unitOfWork;

       
        public BoomServices()
        {
            _unitOfWork = new UnitOfWork();
        }


        public Boom GetEntityById(int Id)
        {
            var boom = _unitOfWork.BoomRepository.GetByID(Id);
            if (boom != null)
            {
                // NA FTIAKSO MAP TA PROPERTIES STO DTO MODELO
            }
            return null;
        }

        public IEnumerable<Boom> GetAllEntities()
        {
            var booms = _unitOfWork.BoomRepository.GetAll().ToList();
            if (booms.Any())
            {
                // NA FTIAKSO MAP TA PROPERTIES STO DTO MODELO
            }
            return null;
        }

        public int CreateEntity(Boom Entity)
        {
            using (var scope = new TransactionScope())
            {
                var entity = new Boom
                {
                    id = Entity.id
                };

                _unitOfWork.BoomRepository.Insert(entity);
                _unitOfWork.Save();
                scope.Complete();
                return entity.id;
            }
        }


        public bool UpdateEntity(int Id, Boom Entity)
        {
            var success = false;
            if (Entity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var entity = _unitOfWork.BoomRepository.GetByID(Id);
                    if (entity != null)
                    {
                        
                        _unitOfWork.BoomRepository.Update(entity);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool DeleteEntity(int Id)
        {
            var success = false;
            if (Id > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var e = _unitOfWork.BoomRepository.GetByID(Id);
                    if (e != null)
                    {

                        _unitOfWork.BoomRepository.Delete(e);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}