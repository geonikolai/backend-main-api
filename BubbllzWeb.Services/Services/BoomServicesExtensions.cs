﻿
using BubbllzWeb.Domain.AppContext;
using BubbllzWeb.Services.DTO;
using BubbllzWeb.Services.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Services.Services
{
    // TA EXTEND ACTIONS GIA TO BOOM
    public class BoomServicesExtensions : BoomServices
    {

        private readonly UnitOfWork _unitOfWork;

        public BoomServicesExtensions()
        {
            _unitOfWork = new UnitOfWork();
        }

        //public async Task<BoomDTO> GetPendingBubbllz(int userId)
        //{
            

        //    using (MysqlContext context = new MysqlContext())
        //    {
        //        Task<BoomDTO> boom = (from a in context.boom.Where(a => a.boom_user_id == userId)
        //                              select new BoomDTO
        //                              {
        //                                  id = a.id,
        //                                  boom_user_id = a.boom_user_id,
        //                                  status = a.status,
        //                                  store_id = a.store_id,
        //                              }).FirstOrDefaultAsync();
        //        return await boom;
        //    }
        //}

        public async Task<IEnumerable<BoomDTO>> GetPendingBubbllz(int userId)
        {
            //var context = _unitOfWork.BoomRepository.Context;
            using (MysqlContext context = new MysqlContext())
            {
                var boom = (from a in context.boom.Where(a => a.boom_user_id == userId)
                            select new BoomDTO
                            {
                                id = a.id,
                                boom_user_id = a.boom_user_id,
                                status = a.status,
                                store_id = a.store_id,
                            }).ToListAsync();

                return await boom;
            }
        }


        public async Task<IEnumerable<BoomDTO>> HasPendingBubbllz(int storeId, int userId)
        {

            using (MysqlContext context = new MysqlContext())
            {
                var boom = (from a in context.boom.Where(a => a.boom_user_id == userId && a.store_id == storeId && a.status == 0)
                            select new BoomDTO
                            {
                                id = a.id,
                                boom_user_id = a.boom_user_id,
                                status = a.status,
                                store_id = a.store_id,
                            }).ToListAsync();
                return await boom;
            }
        }

        public async Task<BoomDTO> CreateNewBubbll(int storeId, int checkinId, int userId, string message, int offerId = -1, string offerType = "")
        {
            var result = await HasPendingBubbllz(storeId, userId);
            var counter = result.Count();
            if (counter > 0)
            {
                throw new Exception("The user has a pending bubbll on store.");
            }

            BoomDTO newBoom = new BoomDTO();
            newBoom.boom_user_id = userId;
            newBoom.store_id = storeId;
            newBoom.checkin_id = checkinId;



            using (MysqlContext context = new MysqlContext())
            {
                Task<BoomDTO> boom = (from a in context.boom.Where(a => a.boom_user_id == userId && a.store_id == storeId)
                                      select new BoomDTO
                                      {
                                          id = a.id,
                                          boom_user_id = a.boom_user_id,
                                          status = a.status,
                                          store_id = a.store_id,
                                      }).FirstOrDefaultAsync();
                return await boom;
            }
        }
    }
}
