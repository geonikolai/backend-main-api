﻿using BubbllzWeb.Domain.AppContext;
using BubbllzWeb.Domain.DbModels;
using BubbllzWeb.Services.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace BubbllzWeb.Services.Services
{
    public class BoomUserServices : IBoomUserService
    {
        private readonly UnitOfWork _unitOfWork;

       
        public BoomUserServices()
        {
            _unitOfWork = new UnitOfWork();
        }

      

        public BoomUser GetEntityById(int Id)
        {
            var boom = _unitOfWork.BoomRepository.GetByID(Id);
            if (boom != null)
            {
                // NA FTIAKSO MAP TA PROPERTIES STO DTO MODELO
            }
            return null;
        }

        public IEnumerable<BoomUser> GetAllEntities()
        {
            var booms = _unitOfWork.BoomRepository.GetAll().ToList();
            if (booms.Any())
            {
                // NA FTIAKSO MAP TA PROPERTIES STO DTO MODELO
            }
            return null;
        }

        public int CreateEntity(BoomUser Entity)
        {
            using (var scope = new TransactionScope())
            {
                var e = new Boom
                {
                    id = Entity.id
                };

                _unitOfWork.BoomRepository.Insert(e);
                _unitOfWork.Save();
                scope.Complete();
                return e.id;
            }
        }

    
        public bool UpdateEntity(int Id, BoomUser Entity)
        {
            var success = false;
            if (Entity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var boom = _unitOfWork.BoomRepository.GetByID(Id);
                    if (boom != null)
                    {
                        //boom.boom_likes = boomEntity.boom_likes;
                        _unitOfWork.BoomRepository.Update(boom);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool DeleteEntity(int Id)
        {
            var success = false;
            if (Id > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var product = _unitOfWork.BoomRepository.GetByID(Id);
                    if (product != null)
                    {

                        _unitOfWork.BoomRepository.Delete(product);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}