﻿
using BubbllzWeb.Domain.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Services.Services
{
    public interface IBoomService : IService
    {
        Boom GetEntityById(int Id);
        IEnumerable<Boom> GetAllEntities();
        int CreateEntity(Boom Entity);
        bool UpdateEntity(int boomId, Boom Entity);
        bool DeleteEntity(int Id);
    }
}
