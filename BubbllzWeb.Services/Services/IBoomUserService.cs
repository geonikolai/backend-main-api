﻿
using BubbllzWeb.Domain.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbllzWeb.Services.Services
{
    public interface IBoomUserService : IService
    {
        BoomUser GetEntityById(int Id);
        IEnumerable<BoomUser> GetAllEntities();
        int CreateEntity(BoomUser Entity);
        bool UpdateEntity(int Id, BoomUser Entity);
        bool DeleteEntity(int Id);
    }
}
